import React from "react"


const Video = ({ videoSrcURL, videoTitle, ...props }) => (
    <div className="video">
        {/* <iframe
            src={videoSrcURL}
            title={videoTitle}
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            frameBorder="0"
            webkitallowfullscreen="true"
            mozallowfullscreen="true"
            allowFullScreen
        /> */}


        <iframe
            width="100%"
            height="1070px"
            src="https://www.youtube.com/embed/7gIdUwbeb9Q?&autoplay=1&loop=1&playlist=7gIdUwbeb9Q&mute=1"
            frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen>
        </iframe>

    </div>
)
export default Video