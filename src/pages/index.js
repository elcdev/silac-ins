import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"

import SilacEquitableImg from "../assets/silac-equitable.png"

import TimeLineImg from '../images/rebrand-timeline.jpg'

const IndexPage = () => (
  <React.Fragment>

    {/* <iframe
      width="100%"
      height="1070px"
      src="https://www.youtube.com/embed/7gIdUwbeb9Q?&autoplay=1&loop=1&playlist=7gIdUwbeb9Q&mute=1"
      frameborder="0"
      allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
      allowfullscreen>
    </iframe> */}

    <iframe
      src="https://player.vimeo.com/video/442377092?autoplay=1&loop=1&muted=1"
      width="100%"
      height="1070px"
      frameborder="0"
      allow="autoplay;
      fullscreen"
      allowfullscreen></iframe>

    <Layout>

      <a href="http://media.equilife.com/annuity/SILAC-timeline-EXternal.pdf" target="_blank"><img src={TimeLineImg} alt="" /></a>

      <section style={{ textAlign: 'center' }}>
        <Link to="https://www.businesswire.com/news/home/20200831005649/en" target="_blank">View the recent Press Release announcing our name change</Link>
      </section>

      <br></br>
      <br></br>
      <section style={{ textAlign: 'center' }}>
        <p >
          As Utah's oldest active life insurance company, we have been Equitable Life & Casualty since the company was founded in 1935 by the Ross family.
          This year is a milestone year as we celebrate our 85th anniversary. Anniversaries are a time to celebrate where we came from and how far we have come, but they are also to celebrate how we have changed.
          Our identity has evolved since entering the annuity space and will continue to be a strong and trustworthy insurance company.
      </p>
        <p>
          We are proud to announce we will be changing our brand name and corporate identity to <strong>SILAC Insurance Company.</strong>
          The name change is a result of our rebranding effort designed to mirror the growth, strength, and transformation of our company.
          Our focus is to continue to be transparent and provide simple, clean competitive products tied with best-in-class service across the board.
      </p>
        <p>
          As SILAC Insurance Company, we are marrying our old traditions with new traditions,
          while simultaneously moving our new brand forward as a leading fixed annuity insurance carrier.
      </p>
        <p><strong>What to expect?</strong></p>
        <p>
          We will gradually transition to our new name throughout 2020 and will officially be <strong>SILAC Insurance Company on January 1, 2021.</strong>
          There will be no changes to our current structure which includes ownership, staff, location, contact information and tax ID, and we will continue normal operations using our new company name, SILAC Insurance Company.
        If applicable and for the time being, please continue to issue checks to Equitable Life & Casualty Insurance Company until further notice.
      </p>
        <p>
          You will begin to receive communications from us using our transitional logo:
      </p>
        <img src={SilacEquitableImg} />
        <p>
          While we are transitioning to our new company name and new logo, you can continue to visit <a href="https://www.equilife.com/">www.equilife.com</a> to learn more about our annuity products, access all portals and download forms.
        </p>

        <p>
          If you have any questions or concerns, please email us at
          <br></br>
          <a href="mailto:questions@equilife.com">questions@equilife.com</a>
        </p>
      </section>
    </Layout>
  </React.Fragment>
)

export default IndexPage


